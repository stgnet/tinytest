// test tiny85 arduino compatibility library
#include "arduino.h"

#define LED 0

void setup() {
	pinMode(LED, OUTPUT);

	// blink light pattern on boot
	int count = 4;
	while (count--) {
		analogWrite(LED, 255);
		delay(250);
		analogWrite(LED, 0);
		delay(250);
	}
}

void loop() {
	unsigned long now = micros();
	analogWrite(LED, (now/1000)&255);
}
